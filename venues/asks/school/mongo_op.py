


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


import reelity.instrument.moon.connect as moon_connect
moon_connection = moon_connect.start ()
	
	

db = moon_connection ['pocket']

# Specify the current and new collection names
current_collection_name = 'cards'
new_collection_name = 'vibes'

# Rename the collection
db[current_collection_name].rename(new_collection_name, dropTarget=True)