
/*
	{
		name: "feelings ECC 448 2: feel",
		fields:  {
			"showy rhythm": {
				"hexadecimal string": intimate_hexadecimal_string
			},
			"UTF8 story": story,
			"UTF8 performance": performance
		}
	}
*/

import { lap } from '@/fleet/lap'

export async function feel (
	{ change, warehouse }, 
	{ flavor, story, performance }
) {
	const flavor_split = flavor.split (":")	
	const showy_rhythm = flavor_split[1]
	
	if (flavor_split [0] !== "ECC_448_2") {
		console.error ("Unknown flavor kind:", flavor_split [0])
		return;
	}
	
	const proceeds = await lap ({
		envelope: {
			name: "feelings ECC 448 2: feel",
			fields:  {
				"showy rhythm": {
					"hexadecimal string": showy_rhythm
				},
				"UTF8 story": story,
				"UTF8 performance": performance
			}
		}
	});
	
	console.log ({ proceeds })
	
	return proceeds
}