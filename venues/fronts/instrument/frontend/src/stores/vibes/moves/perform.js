
import { lap } from '@/fleet/lap'

export async function perform (
	{ change, warehouse }, 
	{ story }
) {
	let vibe = (await warehouse ()).vibe	
				
	console.log ("perform:", vibe)
	
	try {
		var intimate_hexadecimal_string = vibe ["intimate"] ["hexadecimal string"]						
	}
	catch (exception) {
		console.error (exception)
		
		return {
			
		}
	}
	
	const proceeds = await lap ({
		envelope: {
			name: "performances ECC 448 2: perform",
			fields:  {
				"intimate rhythm": {
					"hexadecimal string": intimate_hexadecimal_string
				},
				"UTF8 string": story
			}
		}
	});
	
	console.log ({ proceeds })
	
	return proceeds
}