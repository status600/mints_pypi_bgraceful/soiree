

import Generate_Card from '@/components/generate_vibe/scenery.vue'
import dashboard from '@/components/dashboard/scenery.vue'
import instruments from '@/components/instruments/scenery.vue'

import navigator from '@/components/navigator/scenery.vue'

import { lap } from '@/fleet/lap'

export default {
	inject: ['vibes_store'],
	
	components: {
		dashboard,
		instruments,
		Generate_Card,
		
		navigator
	},
	
	data () {
		return {
			retrieving_vibes: true,
			
			vibes_alert: false,
			
			vibes: [],
			
			
			// "vibe"
			show: "vibe maker",
			show_vibe: { name: '' },
			
			styles: {
				vibe_styles: {
					margin: '4px 0',
					background: 'linear-gradient(22deg, #0000000f, #000000de)',
					color: 'white',
					
				},
				vibe_labels: {
					textTransform: 'none',
					fontSize: '1.5em'
				}
			}
		}		
	},
	
	methods: {
		async copy_text (copyText) {
			try {
				await window.navigator.clipboard.writeText (copyText);
				alert ("Text copied to clipboard!");
			} 
			catch (err) {
				console.error("Unable to copy text: ", err);
			}
		},
		
		async open_vibe (vibe) {
			console.log ('open_vibe', { vibe })
			
			this.show = "vibe"
			this.show_vibe = vibe;
			
			console.log ('vibes store', this.vibes_store)
			await this.vibes_store.moves.modulate ({ vibe }) 
		},
		
		open_vibe_maker () {
			this.show = "vibe maker"
			this.show_vibe = { name: '' };
		},
		
		async make_example_vibe () {
			this.retrieving_vibes = true;
			
			const seed_hex = [
				"01230123012384567BECDFE78745A60123896DAC79B89876A45A6498D",
				"89B89FB456789AB8567012389ABC741234BECFCBC7D69A568392D6A76"
			].join ("");
			
			const proceeds = await lap ({
				envelope: {
					"name": "vibes: make ECC 448 2",
					"fields": {
						"name": "vibe example 1",
						"seed": seed_hex
					}
				}
			});
			if (proceeds.status !== "pass") { 
				
			}
			
			await this.retrieve_vibes ()
			
			console.log (proceeds);
		},
		
		async retrieve_vibes () {
			console.info ("retrieve_vibes")
			
			this.retrieving_vibes = true;
			
			
			const proceeds = await this.vibes_store.moves.retrieve_vibes ()
			if (proceeds.status !== "pass") { 
				try {
					this.vibes_alert = proceeds.notes;
				}
				catch (exception) {
					this.vibes_alert = "retrieval problem";
				}
				return;
			}
			
			
			this.vibes = proceeds.note.vibes
			this.retrieving_vibes = false;
		}
	},
	
	mounted () {
		this.retrieve_vibes ()
	}
}