



import { createRouter, createWebHistory } from 'vue-router'

import stage from '../regions/stage.vue'

const router = createRouter ({
	history: createWebHistory (import.meta.env.BASE_URL),
	routes: [
		{
			path: '/',
			name: 'stage',
			component: stage
		},
		{
			path: '/reel',
			name: 'reel',
			component: () => import('../regions/reel.vue')
		},
		{
			path: '/moves',
			name: 'moves',
			component: () => import ('../regions/moves/region.vue')
		},
		{
			path: '/barcodes',
			name: 'barcodes',
			component: () => import ('../regions/barcodes/region.vue')
		}
	]
})

export default router
