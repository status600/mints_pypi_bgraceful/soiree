
/*
	import { vibes_store } from '@/stores/vibes'
*/

/*
	inject: ['vibes_store'],
*/

import { make_store } from 'mercantile'

import { lap } from '@/fleet/lap'
	
import { feel } from './moves/feel'
import { perform } from './moves/perform'

export async function vibes_store () {
	return await make_store ({
		warehouse: async function () {	
			return {
				"vibe tapped": false, 
				vibe: {},
				vibes: []
			}
		},

		moves: {
			feel,
			perform,
			
			async modulate (
				{ change, warehouse }, 
				{ vibe }
			) {
				await change ("vibe", vibe)
				await change ("vibe tapped", true)
			},
			
			
			/*
				if (proceeds.status !== "pass") { 
					try {
						this.vibes_alert = proceeds.notes;
					}
					catch (exception) {
						this.vibes_alert = "retrieval problem";
					}
					return;
				}
			*/
			async retrieve_vibes (
				{ change, warehouse }
			) {
				const proceeds = await lap ({
					envelope: {
						"name": "vibes: enumerate",
						"fields": {}
					}
				});
				if (proceeds.status === "pass") { 
					await change ("vibes", proceeds.note.vibes)
				}
				
				return proceeds
			},
			
			async make_vibe (
				{ change, warehouse, moves }
			) {
				console.log ("make_vibe:", { moves })
				
				const proceeds = await lap ({
					envelope: {
						"name": "vibes: make ECC 448 2",
						"fields": {
							"name": this.name,
							"seed": this.seed_hex
						}
					}
				});
				if (proceeds.status === "pass") { 
					// await change ("vibes", proceeds.note.vibes)
				}
				
				return proceeds
			}
		},
		
		once_at: {
			async start ({ moves, warehouse }) {
				console.log ("start function called")					
			},
			async demo ({ moves, warehouse }) {
				console.log ("demo function called")					
			}
		}			
	})	
}