******

Bravo!  You have received a Mercantilism Diploma in "status_600" from   
the Simulated Parallel Science Universe Fictional Fantasy Orbital Convergence University   
International Air and Water Embassy Naturalization Department of the Mango Planet
(the planet that is one ellipse closer to the Sun than Earth's ellipse).    

You are now officially certified to perform "status_600" operations in your   
simulated parallel science universe fictional fantasy practice!    

Encore! Encore! Encore! Encore!

******


# status_600
for motion picture use, etc.   

---

## description
Welcome to status_600.

---

## install
```
[xonsh] pip install status_600
```

### MongoDB (Community Edition) also needs to be installed  
https://www.mongodb.com/docs/manual/administration/install-community/   

--

## tutorial
```
[xonsh] status_600 tutorial
```
--

## instrument
```
[xonsh] status_600 instrument layer start --name instrument-1
```

---

## stage
```
[xonsh] status_600 stage layer start --name stage-1
```

---

## vibe generator
This can be found in the tutorial at "modules/EEC_448_2/tutorial"
