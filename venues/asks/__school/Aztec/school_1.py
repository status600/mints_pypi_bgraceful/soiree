





def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../modules',
	'../../../modules_pip'
])

from aztec_code_generator import AztecCode

data = 'Aztec Code 2D :)'
aztec_code = AztecCode (data)

print (aztec_code)
aztec_code.save ('aztec_code.png', module_size=4, border=1)