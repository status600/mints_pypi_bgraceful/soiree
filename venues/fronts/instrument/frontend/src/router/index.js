


import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../places/home.vue'

const router = createRouter({
	history: createWebHistory (import.meta.env.BASE_URL),
	routes: [
		{
			path: '/',
			name: 'home',
			component: HomeView
		},
		{
			path: '/vibes',
			name: 'vibes',
			component: () => import ('@/places/vibes/room.vue')
		},
		{
			path: '/perform',
			name: 'perform',
			component: () => import ('@/places/perform.vue')
		},
		{
			path: '/feel',
			name: 'feel',
			component: () => import ('@/places/feel.vue')
		},
		{
			path: '/subconscious',
			name: 'subconscious',
			component: () => import ('@/places/subconscious.vue')
		},
		{
			path: '/moves',
			name: 'moves',
			component: () => import ('@/places/moves/region.vue')
		},
		{
			path: '/royalty',
			name: 'royalty',
			component: () => import ('@/places/royalty/region.vue')
		}
	]
})

export default router
