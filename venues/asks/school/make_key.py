

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])


import soiree.instrument.moon.pocket.cards.make_ECC_448_2 as make_ECC_448_2
moon_connection = make_ECC_448_2.start (
	name = "card 1",
	seed = "01230123012384567BECDFE78745A60123896DAC79B89876A45A6498D89B89FB456789AB8567012389ABC741234BECFCBC7D69A568392D6A76"
)

